@user_controller
Feature: User controller

  Background:
    * def data = read('classpath:services/user/data/'+ env +'.yaml')
    * def support_data = read('classpath:services/support/data/data.yaml')
    * def authorization = support_data.user.authorization

  @create_user
  Scenario Outline: Create a new user on /public-api/users with code <code>
    * def uuid = function(){ return java.util.UUID.randomUUID()}
    * def name = karate.get('name', <name_value>)
    * def email = karate.get('email', 'qa_faixa_preta' + uuid() + '@gmail.com')
    * def gender = karate.get('gender',<gender_value>)
    * def status = karate.get('status',<status_value>)
    * def body = read('classpath:services/user/payload/user.json')
    Given url url_public_api
    And path '/public-api/users'
    And header Authorization = authorization
    And request body
    When method post
    Then status <status_code>
    And match response contains {code: <code>}
    And match response.data contains data.user.object_user

    @negative
    Examples:
      | name_value       | gender_value | status_value | status_code | code |
      | 'QA Faixa Preta' | 'Female'     | 'Active'     | 200         | 201  |