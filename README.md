# karate_api_automation

Karate DSL is a tool developed in Java that allows the development and execution of API tests using a syntax similar to that of Gherkin.

## Dependencies

* Maven
* Java 8 (Open SDK)

## Setup

* Clone project:
`$ https://gitlab.com/automation-api-karate-medium/automation-api-karate-medium-post`

* Install all dependencies (pom.xml)
`mvn install`

## Test Execution

### Environments

* dev

>##### All tests with Maven
`$ mvn clean test -DargLine="-Dkarate.env=dev"`

>#### Run tests by tag
`$ mvn clean test -DargLine="-Dkarate.env=dev" "-Dkarate.options==--tags=@user_controller"`

## Test Report

* Karate Report: `/target/surefire-reports/karate-summary.html`